=========================================================================================================
=======================			EXCESSO DE VELOCIDADE 			=========================
=========================================================================================================

// PARAMETROS
>SUT00,QCT10,7,15,0061,0100< 											// Velocidade entre 61 e 250km/h	(Disparador == UT00)

// EVENTOS
>SED101 UT00++ RR01-- ZG0 V3 {SCT30 VUT00}{VSBP0,10,0,1}<					// Inicio do evento (Bip unico Ativo)
>SED102 UT00-- RR01-- ZG0 V2 {SCT20 VCT30}{VSBP0,0,0,1}<					// Fim do evento

=========================================================================================================
=======================			LIMITE RPM 		(DESATIVADO) =========================
=========================================================================================================

// PARAMETROS
//>SUT02,QCT11,7,15,9999,9999<											// RPM entre 2201 e 2800 RPM		(Disparador == UT02)

//EVENTOS
//>SED103 UT02++ RR01-- ZG0 V3 {SSH011}{SCT31 VUT02}{VSBP0,10,0,1}< 	// Inicio do evento (Bip unico Ativo)
//>SED104 UT02-- RR01-- ZG0 V2 {SSH010}{SCT20 VCT31}{VSBP0,0,0,1}< 		// Fim do evento

=========================================================================================================
=======================			MARCHA LENTA EXCESSIVA 			=========================
=========================================================================================================

// PARAMETROS
>SUT04,QCT10,7,15,0000,0000<											// Velocidade 0km/h			(Disparador == UT04)
>SUT05,QCT11,7,15,0001,0800<											// RPM entre 1 e 800 RPM	(Disparador == UT05)

// EVENTOS
>SED036 UT04++&&UT05++ RR01-- SGN NN {SCC01 300}{SSH021}<					// Inicia contador	(Retirado UTS 04 & 05 da condicional)
>SED105 SG02++&&CC01++ UT04++&&UT05++&&RR01-- ZG0 V3 {SSH020}{VSBP0,10,0,1}<	// Inicio do evento (Ajuste da flag SG02 com timer)(Bip continuo Ativo)
>SED106 UT04--||UT05-- SG02-- ZG0 V2 {VSBP0,0,0,1}< 					// Fim do Evento (Trocado V3 para V2)

=========================================================================================================
=======================			ALTA ROTACAO 1 				=========================
=========================================================================================================

// PARAMETROS
>SUT14,QCT10,7,15,0000,0020<											// Velocidade entre 0 e 20km/h		(Disparador == UT14)
>SUT15,QCT11,7,15,1600,2800<											// RPM entre 1601 e 2800 RPM		(Disparador == UT15)

// EVENTOS
>SED123 UT15++&&UT14++ RR01-- ZG0 V3 {SCT31 VUT15}{VSBP0,10,0,1}<           // Inicio do evento (Ajuste da cerca RR01 ) (Apagado flag SG06 ques estava sendo usada) (Bip unico Ativo)
>SED124 UT14--||UT15-- RR01-- ZG0 V2 {SCT20 VCT31}{VSBP0,0,0,1}<			// Fim do evento  (Apagado flag SG06 ques estava sendo usada)

=========================================================================================================
=======================			ALTA ROTACAO 2 				=========================
=========================================================================================================

// PARAMETROS
>SUT06,QCT10,7,15,0000,0060<														// Velocidade entre 0 e 60km/h		(Disparador == UT06)
>SUT07,QCT11,7,15,1800,2800<														// RPM entre 1800 e 2800 RPM		(Disparador == UT07)

// EVENTOS
>SED111 UT07++&&UT06++ RR01-- ZG0 V3 {SSH041}{SCT31 VUT07}{VSBP0,10,0,1}< 	// Inicio do evento (Apagado condicional) (Bip unico Ativo) 
>SED112 UT06--||UT07-- RR01-- ZG0 V2 {SSH040}{SCT20 VCT31}{VSBP0,0,0,1}< 			// Fim do evento (Apagado condicional)

=========================================================================================================
=======================			EXCESSO DE ROTACAO (DESATIVADO)		=========================
=========================================================================================================

// PARAMETROS
//>SUT08,QCT11,7,15,2001,2800< 											// RPM entre 2001 e 2800 RPM		(Disparador == UT08)

//EVENTOS
//>SED114 UT08++&&UT26++ RR01-- ZG0 V3 {SCT20 VCT31}{VSBP0,10,0,1}<					// Fim do evento (Retirado flag e condicional) (Trocado V3 e V2)
//>SED113 UT08-- RR01-- ZG0 V2 {SCT31 VUT08}{VSBP0,0,0,1}< 				// Inicio do evento (Retirado flag e condicional) (Trocado V3 e V2) (Bip unico Ativo)

=========================================================================================================
=======================			BANGUELA			=================================
=========================================================================================================

// PARAMETROS
>SUT40,QCT10,7,15,0030,0100<											// Velocidade entre 30 e 100 km/h	(Disparador == UV08)
>SUT45,QCT11,7,15,0001,0800<											// RPM entre 0001 e 0800 RPM		(Disparador == UV13)

// EVENTOS
>SED075 UT45++&&UT40++ RR01-- SGN NN {SCC03 5}{SSH101}<							// Inicia contador (O contador tem uma folga de 2segs portanto 5 = 3) 
>SED146 SG10++&&CC03++ UT45++&&UT40++&&RR01-- ZG0 V3 {SCT32 VUT45}{SSH100}< 	// Inicio do evento ap? o contador (Ajustado flag SG10 com timer) (Bip Desativado)
>SED147 UT45--||UT40-- SG10-- ZG0 V2 {SCT20 VCT32}<								// Fim do evento

=========================================================================================================
=======================			FREADA BRUSCA E ACELERACAO BRUSCA	=========================
=========================================================================================================

// PARAMETROS
>SUT10,QAC,4,20,-30,-9< 												// Freada = 9 Km/h/s			(Disparador == UT10)
>SUT11,QAC,4,20,9,20< 													// Aceleracao = 9 Km/h/s		(Disparador == UT11)

// EVENTOS
>SED117 UT10++ IN07++ ZG0 V2 {SCT20 VUT10*-1}{VSBP0,10,0,1}<			// Inicio do evento (Bip unico Ativo) (Retirada flag SG08)
>SED043 UT10-- IN07++ SGN NN {VSBP0,0,0,1}<								// Fim do evento (Retirada flag SG08)	
>SED118 UT11++ IN07++ ZG0 V2 {SCT20 VUT11}{VSBP0,10,0,1}<				// Inicio do evento (Retirada flag SG08) (Bip unico Ativo)	
>SED044 UT11++ IN07++ SGN NN {VSBP0,0,0,1}<								// Fim do evento (Retirada flag SG08)

=========================================================================================================
=======================			PEDAL ACELERADOR EXCESSIVO 			=================
=========================================================================================================

// PARAMETROS
>SUT26,QCT16,7,15,0001,0100<											//Pedal entre 5% e 100%			(Disparador == UU10)
>SUT24,QCT16,7,15,0090,0100<											//Pedal entre 90% e 100%		(Disparador == UU08)
	
// EVENTOS	
>SED125 UT24++ RR01-- ZG0 V3 {SCT20 VCT37}{VSBP0,10,0,1}< 				// Fim do evento (Retirada flag SG09)
>SED126 UT24-- RR01-- ZG0 V2 {SCT37 VUT24}{VSBP0,0,0,1}< 			// Inicio do evento (Retirada flag SG09)(Retirado a condicional)(Bip unico Ativo)

=========================================================================================================
===================================    	KM SEM CONSUMO (DESATIVADO)	   ===============================================
=========================================================================================================

// PARAMETROS
//>SUT42,QCT10,7,15,9999,9999<                    						// Velocidade acima de 15Km/h 		(Disparador == UV10)
//>SUT29,QCT11,7,15,9999,9999<                    						// RPM acima de 850			(Disparador == UU13)

// EVENTOS
//>SED076 UT29++&&UT42++&&UT26-- RR01-- SGN NN {SCC02 6}{SSH111}<			// Inicia contador (O contador tem uma folga de 2segs portanto 6 = 4)
//>SED154 SG11++&&CC02++UT29++ UT42++&&UT26--&&RR01-- ZG0 V3 {SCT31 VUT29}{SSH110}<         // Inicio do evento ap? o contador (Bip Desativado)	
//>SED155 UT29--||UT42--||UT26++ SG11-- ZG0 V2 {SCT20 VCT31}<           // Fim do evento

=========================================================================================================
=======================            	ALTA ROTACAO PARADO (DESATIVADO)	   ==========================================
=========================================================================================================


// PARAMETROS
//>SUT16,QCT10,7,15,0000,0000<                    						// Velocidade igual a 0 		(Disparador == UU00)
//>SUT31,QCT11,7,15,1500,2800<                    						// RPM entre 1500 e 2800 RPM		(Disparador == UU15)

// EVENTOS
//>SED158 UT31++&&UT16++&&UT26++ RR01-- ZG0 V3 {SCT31 VUT31}<               		// Inicio do evento (Bip Desativado) (Retirado flag SG03)(Retirada a condicional)
//>SED159 UT31--||UT16-- RR01-- ZG0 V2 {SCT20 VCT31}<                				// Fim do evento

=========================================================================================================
===================================	BAIXAS ROTACOES	(DESATIVADO)=========================================
=========================================================================================================

// PARAMETROS
//>SUT28,QCT10,7,15,9999,9999<											// Velocidade acima de 30Km/h		(Disparador == UU12)
//>SUT32,QCT11,7,15,9999,9999<											// RPM entre 1 e 1000 RPM		(Disparador == UV00)

// EVENTOS
//>SED096 UT32++&&UT28++ RR01-- SGN NN {SSH121}{SCC07 2}<			// (O contador tem uma folga de 2segs portanto 4 = 2)						
//>SED156 SG12++&&CC07++ UT12++&&UT32++&&RR01-- ZG0 V3 {SSH120}{SCT32 VUT32}<	// Inicio do evento
//>SED157 UT32--||UT28--||UT26-- SG12-- ZG0 V2 {SCT20 VCT32}<			// Fim do evento

=========================================================================================================
=======================			BATERIA BAIXA 	(DESATIVADO)		=================
=========================================================================================================

//>SUT27,QAD,36,4,9999,9999<//


//>SED164 UT27++ +- SGN NN {SCC11 60}{SSH101}<                              // Inicia o timer e levanta a flag
//>SED131 CC11-- UT27++&&SG10++ ZG0 V3 {SSH100}{SCT35 VUT27}{VSBP0,20,10,3}< // Inicio do evento
//>SED132 UT27-- SG10-- ZG0 V2 {SCT24 VCT35}{VSBP0,0,0,1}<                 // Fim do evento

=========================================================================================================
=======================			BATERIA ALTA 	(DESATIVADO)		=================
=========================================================================================================

//>SUT28,QAD,36,4,9999,9999<//


//>SED166 UT28++ +- SGN NN {SCC12 60}{SSH111}<                              // Inicia o timer e levanta a flag
//>SED133 CC12-- UT28++&&SG11++ ZG0 V3 {SSH110}{SCT35 VUT28}{VSBP0,20,10,3}< // Inicio do evento
//>SED134 UT28-- SG11-- ZG0 V2 {SCT24 VCT35}{VSBP0,0,0,1}<                 // Fim do evento

=========================================================================================================
=======================			EXCESSO DE TEMPERATURA DO ARREFECIMENTO		=========================
=========================================================================================================

//>SUT29,QCT19,7,15,9999,9999<												// OBS: AJUSTAR A UT DE ARCODO COM A CT DA CAN

//>SED168 UT29++ +- SGN NN {SCC13 60}{SSH121}<                             // Inicia o timer e levanta a flag
//>SED135 CC13-- UT29++&&SG12++ ZG0 V3 {SSH120}{SCT36 VUT29}{VSBP0,20,10,3}< // Inicio do evento
//>SED136 UT29-- SG12-- ZG0 V2 {SCT24 VCT36}{VSBP0,0,0,1}<                 // Fim do evento

=========================================================================================================
=======================			PRESSÃO DO ÓLEO OCIOSO	(DESATIVADO)	=========================
=========================================================================================================

//>SUT32,QCT25,7,4,9999,9999<												// OBS: AJUSTAR A UT DE ARCODO COM A CT DA CAN

//>SED174 UT32++ +- SGN NN {SCC16 5}{SSH151}<                              // Inicia o timer e levanta a flag
//>SED175 CC16-- UT32++&&SG15++ ZG0 V3 {SSH150}{SCT39 VUT32}{VSBP0,20,10,3}< // Inicio do evento
//>SED143 UT32-- SG15-- ZG0 V2 {SCT24 VCT39}{VSBP0,0,0,1}<                 // Fim do evento

=========================================================================================================
=======================			PRESSÃO DO AR A NA IGNIÇÃO (DESATIVADO)		=========================
=========================================================================================================

//>SED170 UT30++ +- SGN NN {SCC14 420}{SSH131}<                             // Inicia o timer e levanta a flag
//>SED171 CC14-- UT30++&&SG13++ ZG0 V3 {SSH130}{SCT37 VUT30}{VSBP0,20,10,3}< // Inicio do evento
//>SED139 UT30-- SG13-- ZG0 V2 {SCT24 VCT37}{VSBP0,0,0,1}<                 // Fim do evento

=========================================================================================================
=======================			PRESSÃO DO AR B NA IGNIÇÃO	(DESATIVADO)	=========================
=========================================================================================================

//>SED172 UT31++ +- SGN NN {SCC15 420}{SSH141}<                             // Inicia o timer e levanta a flag
//>SED173 CC15-- UT31++&&SG14++ ZG0 V3 {SSH140}{SCT38 VUT31}{VSBP0,20,10,3}< // Inicio do evento
//>SED140 UT31-- SG14-- ZG0 V2 {SCT24 VCT38}{VSBP0,0,0,1}<                 // Fim do evento

